from ..connection.connection import create_connection
from sqlite3 import Error


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class TableVars:
    """
    This is the class for defining the tables
    """

    sql_create_course_category_table = """CREATE TABLE IF NOT EXISTS course_category (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            name text NOT NULL,
                                            type text NOT NULL
                                        );"""

    sql_create_courses_table = """CREATE TABLE IF NOT EXISTS courses (
                                                id integer PRIMARY KEY AUTOINCREMENT,
                                                name text NOT NULL,
                                                duration text NOT NULL,
                                                type text NOT NULL,
                                                course_category_id integer,
                                                popularity integer,
                                                FOREIGN KEY (course_category_id) REFERENCES course_category (id)
                                            );"""

    sql_create_syllabus_table = """CREATE TABLE IF NOT EXISTS syllabus (
                                                    id integer PRIMARY KEY AUTOINCREMENT,
                                                    name text NOT NULL,
                                                    syllabus_code text NOT NULL
                                                );"""

    sql_create_books_table = """CREATE TABLE IF NOT EXISTS books (
                                                        id integer PRIMARY KEY AUTOINCREMENT,
                                                        name text NOT NULL,
                                                        book_code text NOT NULL,
                                                        type text
                                                    );"""

    sql_create_course_syllabus_table = """CREATE TABLE IF NOT EXISTS course_syllabus (
                                                id integer PRIMARY KEY AUTOINCREMENT,
                                                course_id integer not null UNIQUE,
                                                syllabus_id integer not null
                                            );"""

    sql_create_syllabus_books_table = """CREATE TABLE IF NOT EXISTS syllabus_books (
                                                    id integer PRIMARY KEY AUTOINCREMENT,
                                                    book_id integer not null,
                                                    syllabus_id integer not null UNIQUE
                                                );"""

    sql_create_student_table = """CREATE TABLE IF NOT EXISTS student (
                                                id integer PRIMARY KEY AUTOINCREMENT,
                                                name text NOT NULL UNIQUE,
                                                father_name text NOT NULL,
                                                dob text NOT NULL,
                                                address text NOT NULL,
                                                course_id integer NOT NULL,
                                                syllabus_id integer NOT NULL,
                                                enrollment_no text NOT NULL UNIQUE,
                                                FOREIGN KEY (course_id) REFERENCES courses (id),
                                                FOREIGN KEY (syllabus_id) REFERENCES syllabus (id)
                                            );"""


def get_table_data(conn, get_query):
    """
    This is to insert the value to the tables
    :param conn: connection object
    :param insert_query: query for the insert
    :return:
    """
    try:
        conn.row_factory = dict_factory
        c = conn.cursor()
        c.execute(get_query)
        rows = c.fetchall()
        return rows
    except Error as e:
        print(e)
        return '{}'.format(str(e))


def remove_table_entry(conn, get_query):
    """
    This is to insert the value to the tables
    :param conn: connection object
    :param insert_query: query for the insert
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(get_query)
        print(c)
        conn.commit()
        c.close()
        return True
    except Error as e:
        print(e)
        return '{}'.format(str(e))


def insert_table(conn, insert_query, data):
    """
    This is to insert the value to the tables
    :param conn: connection object
    :param insert_query: query for the insert
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(insert_query, data)
        conn.commit()
        c.close()
        return True
    except Exception as e:
        print(e)
        return '{}'.format(str(e))


def create_table(conn, table_name):
    """ This is to create the table for the connection
    :param conn: Connection object for the application
    :param table_name: a CREATE TABLE
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(table_name)
        print(c)
    except Error as e:
        print(e)


def update_table(conn, table_name):
    """ This is to create the table for the connection
    :param conn: Connection object for the application
    :param table_name: a CREATE TABLE
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(table_name)
        print(c)
        conn.commit()
        c.close()
        return True
    except Error as e:
        print(e)


def create_db_conn():
    """
    This function generates creates the table by setting the connections for the different tables
    """
    # create a database connection
    conn = create_connection()

    # create tables
    if conn is not None:
        # create course_category table
        create_table(conn, TableVars.sql_create_course_category_table)

        # create courses table
        create_table(conn, TableVars.sql_create_courses_table)

        # create books table
        create_table(conn, TableVars.sql_create_books_table)

        # create syllabus table
        create_table(conn, TableVars.sql_create_syllabus_table)

        # create course_syllabus table
        create_table(conn, TableVars.sql_create_course_syllabus_table)

        # create student table
        create_table(conn, TableVars.sql_create_student_table)

        # create syllabus_books table
        create_table(conn, TableVars.sql_create_syllabus_books_table)

        return True
    else:
        print("Error! cannot create the database connection.")
        return False
