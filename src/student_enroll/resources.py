from flask import Blueprint
from flask.views import MethodView
from flask import request
# from main import app
import sqlite3
from flask import Flask
from flask import make_response, jsonify
from ..db.sql import create_db_conn, insert_table, get_table_data, remove_table_entry, update_table
from ..connection.connection import create_connection

bp = Blueprint('pos', __name__, url_prefix='/api/v1')


@bp.route("/create_tables", methods=['POST'])
def create_tables():
    tables = create_db_conn()
    if tables:
        return make_response(jsonify({'message': 'Tables created successfully', 'status': 200}), 200)
    else:
        return make_response(jsonify({'message': 'Tables not created properly', 'status': 422}), 422)


class StudentView(MethodView):

    def get(self):
        args = request.environ
        if 'HTTP_AUTHORIZATION' not in args:
            return make_response(jsonify({"message": "Authorization token is missing", "status": 401}), 401)
        data = request.args
        course_query = None
        syllabus_query = None
        if 'course_name' in data:
            course_query = "SELECT id FROM courses WHERE name='{}';".format(data['course_name'])
        elif 'syllabus_name' in data:
            syllabus_query = "SELECT id FROM syllabus WHERE name='{}';".format(data['syllabus_name'])
        conn = create_connection()
        try:
            if course_query:
                course_data = get_table_data(conn, course_query)
                course_id = 0
                for c in course_data:
                    course_id = c['id']
                sqlite_get_student = "SELECT * FROM student WHERE course_id = {};".format(course_id)
            elif syllabus_query:
                syllabus_data = get_table_data(conn, syllabus_query)
                syllabus_id = 0
                for s in syllabus_data:
                    syllabus_id = s['id']
                sqlite_get_student = "SELECT * FROM student WHERE syllabus_id = {};".format(syllabus_id)
            elif 'enrollment_no' in data:
                sqlite_get_student = "SELECT * FROM student where enrollment_no = {};".format(data['enrollment_no'])
            else:
                sqlite_get_student = "SELECT * FROM student;"

            inserted = get_table_data(conn, sqlite_get_student)
            if inserted:
                return make_response(jsonify(inserted)), 200
            else:
                return make_response(jsonify({"message": str(inserted), "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")

    def post(self):
        data = request.json
        if 'name' not in data:
            return make_response(jsonify({"message": "name is required", "status": 401}), 401)
        if 'father_name' not in data:
            return make_response(jsonify({"message": "father_name is required", "status": 401}), 401)
        if 'dob' not in data:
            return make_response(jsonify({"message": "dob is required", "status": 401}), 401)
        if 'address' not in data:
            return make_response(jsonify({"message": "address is required", "status": 401}), 401)
        if 'course_id' not in data:
            return make_response(jsonify({"message": "course_id is required", "status": 401}), 401)
        if 'syllabus_id' not in data:
            return make_response(jsonify({"message": "syllabus_id is required", "status": 401}), 401)
        if 'enrollment_no' not in data:
            return make_response(jsonify({"message": "enrollment_no is required", "status": 401}), 401)

        conn = create_connection()
        try:
            sqlite_insert_with_param = """INSERT INTO student
                                      (name, father_name, dob, address, course_id, syllabus_id, enrollment_no) 
                                      VALUES (?, ?, ?, ?, ?, ?, ?);"""

            data_tuple = (data['name'], data['father_name'], data['dob'], data['address'], data['course_id'],
                          data['syllabus_id'], data['enrollment_no'])
            inserted = insert_table(conn, sqlite_insert_with_param, data_tuple)
            if inserted is True:
                return make_response(jsonify({"message": 'Data inserted for images successfully', "status": 200})), 200
            else:
                return make_response(jsonify({"message": str(inserted), "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")

    def put(self):
        data = request.json
        args = request.args
        if 'id' not in args:
            return make_response(jsonify({"message": "Please pass id in the params for update", "status": 422}), 422)
        slug = args['id']
        conn = create_connection()
        try:
            if 'name' in data:
                name = data['name']
                name_update = """UPDATE syllabus SET name = '{}' WHERE id = {}""".format(name, slug)
                update_table(conn, name_update)

            if 'father_name' in data:
                father_name = data['father_name']
                father_name_update = """UPDATE syllabus SET father_name = '{}' WHERE id = {}""".format(father_name, slug)
                update_table(conn, father_name_update)
            if 'dob' in data:
                dob = data['dob']
                dob_update = """UPDATE syllabus SET dob = '{}' WHERE id = {}""".format(dob, slug)
                update_table(conn, dob_update)
            if 'address' in data:
                address = data['address']
                address_update = """UPDATE syllabus SET address = '{}' WHERE id = {}""".format(address, slug)
                update_table(conn, address_update)
            if 'course_id' in data:
                course_id = data['course_id']
                course_update = """UPDATE syllabus SET course_id = '{}' WHERE id = {}""".format(course_id, slug)
                update_table(conn, course_update)
            if 'syllabus_id' in data:
                syllabus_id = data['syllabus_id']
                syllabus_update = """UPDATE syllabus SET syllabus_id = '{}' WHERE id = {}""".format(syllabus_id, slug)
                update_table(conn, syllabus_update)
            if 'enrollment_no' in data:
                enrollment_no = data['enrollment_no']
                enroll_update = """UPDATE syllabus SET enrollment_no = '{}' WHERE id = {}""".format(enrollment_no, slug)
                update_table(conn, enroll_update)

            return make_response(jsonify({"message": 'Data updated successfully', "status": 200})), 200

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")

    def delete(self, slug=None):
        data = request.environ
        if 'HTTP_AUTHORIZATION' not in data:
            return make_response(jsonify({"message": "Authorization token is missing", "status": 401}), 401)

        args = request.args
        if 'id' not in args:
            return make_response(jsonify({"message": "Please pass id in the params for update", "status": 422}), 422)
        slug = args['id']
        conn = create_connection()
        try:
            sqlite_get_with_param = "DELETE FROM student WHERE id={};".format(slug)

            inserted = remove_table_entry(conn, sqlite_get_with_param)
            if inserted is True:
                return make_response(jsonify({"message": "Record Deleted successfully", "status": inserted})), 200
            else:
                return make_response(jsonify({"message": "No data found", "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")


bp.add_url_rule('/student', view_func=StudentView.as_view('student'))


class SyllabusView(MethodView):

    def get(self):
        args = request.environ
        if 'HTTP_AUTHORIZATION' not in args:
            return make_response(jsonify({"message": "Authorization token is missing", "status": 401}), 401)
        data = request.args
        conn = create_connection()
        try:
            if 'syllabus_name' in data:
                sqlite_get_student = "SELECT * FROM syllabus WHERE name = {};".format(data['syllabus_name'])
            else:
                sqlite_get_student = "SELECT * FROM syllabus;"

            inserted = get_table_data(conn, sqlite_get_student)
            if inserted:
                return make_response(jsonify(inserted)), 200
            else:
                return make_response(jsonify({"message": str(inserted), "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")

    def post(self):
        data = request.json
        if not data:
            return make_response(jsonify({"message": "JSON body is empty", "status": 401}), 401)
        if 'name' not in data:
            return make_response(jsonify({"message": "name is required", "status": 401}), 401)
        if 'syllabus_code' not in data:
            return make_response(jsonify({"message": "syllabus_code is required", "status": 401}), 401)

        conn = create_connection()
        try:
            sqlite_insert_with_param = """INSERT INTO syllabus
                                      (name, syllabus_code)
                                      VALUES (?, ?);"""

            data_tuple = (data['name'], data['syllabus_code'])
            inserted = insert_table(conn, sqlite_insert_with_param, data_tuple)
            if inserted is True:
                return make_response(jsonify({"message": 'Data inserted for images successfully', "status": 200})), 200
            else:
                return make_response(jsonify({"message": str(inserted), "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")

    def put(self):
        data = request.json
        args = request.args
        if 'id' not in args:
            return make_response(jsonify({"message": "Please pass id in the params for update", "status": 422}), 422)
        name = None
        syllabus_code = None
        if 'name' in data:
            name = data['name']
        if 'syllabus_code' in data:
            syllabus_code = data['syllabus_code']
        conn = create_connection()
        try:
            if name and syllabus_code:
                sqlite_insert_with_param = """UPDATE syllabus SET name = '{}', syllabus_code = '{}'
                                                WHERE id = {}""".format(name, syllabus_code, args['id'])
                print(sqlite_insert_with_param)
            elif name:
                sqlite_insert_with_param = """UPDATE syllabus SET name = '{}',
                                                WHERE id = {}""".format(name, args['id'])
            else:
                sqlite_insert_with_param = """UPDATE syllabus SET syllabus_code = '{}',
                                                WHERE id = {}""".format(syllabus_code, args['id'])

            inserted = update_table(conn, sqlite_insert_with_param)
            if inserted is True:
                return make_response(jsonify({"message": 'Data updated successfully', "status": 200})), 200
            else:
                return make_response(jsonify({"message": str(inserted), "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")

    def delete(self):
        auth = request.environ
        args = request.args
        if 'id' not in args:
            return make_response(jsonify({"message": "Please pass id in the params for update", "status": 422}), 422)
        slug = args['id']
        if 'HTTP_AUTHORIZATION' not in auth:
            return make_response(jsonify({"message": "Authorization token is missing", "status": 401}), 401)

        if not slug:
            return make_response(jsonify({"message": "Please pass id in the slug", "status": 422}), 422)
        conn = create_connection()
        try:
            sqlite_get_with_param = "DELETE FROM syllabus WHERE id={};".format(slug)

            inserted = remove_table_entry(conn, sqlite_get_with_param)
            if inserted is True:
                return make_response(jsonify({"message": "Record Deleted successfully", "status": inserted})), 200
            else:
                return make_response(jsonify({"message": "No data found", "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")


bp.add_url_rule('/syllabus', view_func=SyllabusView.as_view('syllabus'))


class CourseView(MethodView):

    def get(self):
        args = request.environ
        if 'HTTP_AUTHORIZATION' not in args:
            return make_response(jsonify({"message": "Authorization token is missing", "status": 401}), 401)
        data = request.args
        conn = create_connection()
        try:
            if 'course_name' in data:
                sqlite_get_course = "SELECT * FROM courses WHERE name = {};".format(data['course_name'])
            else:
                sqlite_get_course = "SELECT * FROM courses;"

            inserted = get_table_data(conn, sqlite_get_course)
            if inserted:
                return make_response(jsonify(inserted)), 200
            else:
                return make_response(jsonify({"message": str(inserted), "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")

    def post(self):
        data = request.json
        if 'name' not in data:
            return make_response(jsonify({"message": "name is required", "status": 401}), 401)
        if 'duration' not in data:
            return make_response(jsonify({"message": "duration is required", "status": 401}), 401)
        if 'type' not in data:
            return make_response(jsonify({"message": "type is required", "status": 401}), 401)
        if 'course_category_id' not in data:
            return make_response(jsonify({"message": "type is required", "status": 401}), 401)

        conn = create_connection()
        try:
            if 'popularity' in data:
                sqlite_insert_with_param = """INSERT INTO courses
                                                      (name, duration, type, course_category_id, popularity)
                                                      VALUES (?, ?, ?, ?, ?);"""

                data_tuple = (data['name'], data['duration'], data['type'], data['course_category_id'],
                              data['popularity'])
            else:
                sqlite_insert_with_param = """INSERT INTO courses
                                          (name, duration, type, course_category_id)
                                          VALUES (?, ?, ?, ?);"""

                data_tuple = (data['name'], data['duration'], data['type'], data['course_category_id'])
            inserted = insert_table(conn, sqlite_insert_with_param, data_tuple)
            if inserted is True:
                return make_response(jsonify({"message": 'Data inserted successfully', "status": 200})), 200
            else:
                return make_response(jsonify({"message": str(inserted), "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")

    def put(self):
        data = request.json
        args = request.args
        if 'id' not in args:
            return make_response(jsonify({"message": "Please pass id in the params for update", "status": 422}), 422)
        slug = args['id']
        conn = create_connection()
        try:
            if 'name' in data:
                name = data['name']
                name_update = """UPDATE courses SET name = '{}' WHERE id = {}""".format(name, slug)
                update_table(conn, name_update)
            if 'duration' in data:
                duration = data['duration']
                duration_update = """UPDATE courses SET duration = {} WHERE id = {}""".format(int(duration), slug)
                update_table(conn, duration_update)
            if 'type' in data:
                type = data['type']
                type_update = """UPDATE courses SET type = '{}' WHERE id = {}""".format(type, slug)
                update_table(conn, type_update)
            if 'course_category_id' in data:
                course_category_id = data['course_category_id']
                course_category_update = """UPDATE courses SET course_category_id = '{}' WHERE id = {}"""\
                    .format(course_category_id, slug)
                update_table(conn, course_category_update)
            if 'popularity' in data:
                popularity = data['popularity']
                popularity_update = """UPDATE courses SET popularity = '{}' WHERE id = {}""".format(popularity, slug)
                update_table(conn, popularity_update)

            return make_response(jsonify({"message": 'Data updated successfully', "status": 200})), 200

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")

    def delete(self):
        data = request.environ
        if 'HTTP_AUTHORIZATION' not in data:
            return make_response(jsonify({"message": "Authorization token is missing", "status": 401}), 401)

        args = request.args
        if 'id' not in args:
            return make_response(jsonify({"message": "Please pass id in the params for update", "status": 422}), 422)
        slug = args['id']
        conn = create_connection()
        try:
            sqlite_get_with_param = "DELETE FROM courses WHERE id={};".format(slug)

            inserted = remove_table_entry(conn, sqlite_get_with_param)
            if inserted is True:
                return make_response(jsonify({"message": "Record Deleted successfully", "status": inserted})), 200
            else:
                return make_response(jsonify({"message": "No data found", "status": 422})), 422

        except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
        finally:
            if conn:
                conn.close()
                print("The SQLite connection is closed")


bp.add_url_rule('/course', view_func=CourseView.as_view('course'))
